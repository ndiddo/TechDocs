---
layout: default
title: "Sega H1"
---

# Sega H1
![](ReferenceImage.png)

# Status
I have yet to acquire this hardware, or gather any real information on it.

# Overview
Refer to [SegaRetro](https://segaretro.org/Sega_System_24) for a general hardware overview and list of games. The only real information you'll find on this system is what's documented in the [MAME source](https://github.com/mamedev/mame/blob/master/src/mame/drivers/coolridr.cpp). About the only observation I have which appears to not be written down elsewhere is that the communication board appears to only have been present on Cool Riders, not Aqua Stage. If I can get my hands on the hardware, I'll do some work on this system. It seems likely that Sega had big plans for this system when it was designed, but ultimately the system was abandoned shortly after it was developed.

# Miscellaneous Photos
Here's a collection of images of this system that have been posted online from various sources: {% include folder title="Hardware Photos" target="Pictures" %}
