---
layout: default
title: "Copying System 24 Disks"
pageGroup: "SegaSystem24"
---

# Overview
If you're the proud owner of a Sega System 24 arcade system with a floppy drive, you may have had cause in the past to back up the disk. The system itself has a disk copy feature, but by all accounts it's a bit hard to do on the real system, requiring you to push bits of your system out of the way, and have five hands and be in three places at once. Doing this on a removed board outside the system is much easier, but it still just gives you another disk at the end, not a digital backup on a PC.
What if you don't even have a working disk to start with? If you're unlucky enough to have a System 24 board that has a damaged disk, or is missing the disk entirely, you'll certainly have wondered how to create a new disk without an original floppy to use.

This is the situation I was in, where I acquired a System 24 pcb which had the FD1094 for Crackdown, and included the FDD mounted together on a board, but for some reason the disk itself was missing. MAME had disk backups, so I thought it must be fairly simple to just take those disk images and write them to a new floppy, right? Unfortunately, it's not as easy as you'd think. The Sega System 24 uses standard 3 1/2" disks, but they're formatted to hold around 1.8MB, with an unusual non-uniform sector size. PC drives can theoretically read this format but it requires talking directly to the floppy controller, and few PC programs do that, with fewer still having the necessary options and flexibility to work with these disks.

After scouring the web for any instructions on how to re-create System 24 disks from a PC and drawing a blank, I set out to find a solution. It took two days of tinkering with dozens of different programs between DOS and Windows, and in the end I had to write a conversion tool myself to pull it off, but it is possible to do this in a simple way on a reasonably modern PC. On this page, I'll show you how to copy System 24 disks, back them up to PC, create equivalent MAME disk images, and take the MAME disk images and create real floppy disks from them. All you need is a Windows-based PC with a "real" (IE, motherboard provided, not USB) floppy disk port, with a standard 1.44MB FDD installed. Note that you do not need special floppy disks, as any standard 1.44MB floppy should be able to be formatted to hold up to 2MB of data. I used Verbatim DataLife MF 2HD disks in my attempts.


# Backing up a disk to PC (.dsk file)
1. Download and install the [fdrawcmd.sys](http://simonowen.com/fdrawcmd) floppy filter driver ([direct](http://simonowen.com/fdrawcmd/FdInstall.exe), {% include download title="mirror" target="Utilities/FdInstall.exe" %}). This gives low-level access to the floppy controller.
2. Download and extract [SAMdisk 3.8.10](http://simonowen.com/samdisk) ([direct](http://simonowen.com/samdisk/SAMdisk3810.zip), {% include download title="mirror" target="Utilities/SAMdisk3810.zip" %}). Newer versions may work better, but this was the version tested.
3. Run the following command:
```shell
samdisk.exe a: "[filename].dsk" -c80
```

That's it! This will do all the work required in order to back up a System 24 floppy.

If you want to "overdump" the disk (IE, read any additional sectors that might be present), remove the `-c80` from the command line. This would be a good idea for original disks, but for copied disks it might return factory filled sectors up to the end of the disk.

Note that this image format is NOT directly compatible with MAME. It contains additional metadata to describe the sector format and other properties of the physical media. This is a good thing if you want a useful disk image for archival purposes. This format is more complete than the raw data dumps in MAME. If you want to convert these .dsk files into the raw .img sector dump format used in MAME though, read on. Instructions are given below.

# Creating a disk from PC backup (.dsk file)
1. Download and install the [fdrawcmd.sys](http://simonowen.com/fdrawcmd) floppy filter driver ([direct](http://simonowen.com/fdrawcmd/FdInstall.exe), {% include download title="mirror" target="Utilities/FdInstall.exe" %}). This gives low-level access to the floppy controller.
2. Download and extract [SAMdisk 3.8.10](http://simonowen.com/samdisk) ([direct](http://simonowen.com/samdisk/SAMdisk3810.zip), {% include download title="mirror" target="Utilities/SAMdisk3810.zip" %}). Newer versions may work better, but this was the version tested.
3. Run the following command:
```shell
samdisk.exe "[filename].dsk" a:
```

That's it! This will create a system 24 floppy disk from an image that was created by SAMdisk.


# Converting a .dsk file to a MAME compatible .img file
1. Download and extract the [HxC Floppy Emulator software](http://hxc2001.com/download/floppy_drive_emulator) ([direct](http://hxc2001.com/download/floppy_drive_emulator/HxCFloppyEmulator_soft.zip), {% include download title="mirror" target="Utilities/HxCFloppyEmulator_soft.zip" %}).
2. Run "HxCFloppyEmulator.exe"
3. Select "Floppy image->Load" from the menu, and select your .dsk file.
4. Select "Floppy image->Export disk/Save As" from the menu, change the "Save as type" to ".img", enter a filename, and press "Save".

You now have an .img file equivalent to what you would have obtained through "Anadisk", which is what was originally used to create the MAME images. If you want instructions for using Anadisk, read on, as they'll be given later.

# Creating a disk from MAME rom (.img file)
1. Download and install the [fdrawcmd.sys](http://simonowen.com/fdrawcmd) floppy filter driver ([direct](http://simonowen.com/fdrawcmd/FdInstall.exe), {% include download title="mirror" target="Utilities/FdInstall.exe" %}). This gives low-level access to the floppy controller.
2. Download and extract [SAMdisk 3.8.10](http://simonowen.com/samdisk) ([direct](http://simonowen.com/samdisk/SAMdisk3810.zip), {% include download title="mirror" target="Utilities/SAMdisk3810.zip" %}). Newer versions may work better, but this was the version tested.
3. Download and extract my InterleaveSystem24 tool ([binary](Utilities/InterleaveSystem24.zip), [source](Utilities/InterleaveSystem24Src.zip)).
4. Obtain the MAME rom with the disk you want to create. In my case, it was "crkdownu.zip".
5. Extract the disk image from the MAME rom. In my case, it was "ds3-5000-04d.img".
6. Open a Windows command prompt
7. Convert the MAME disk image to an ".s24" image using the following command:
```shell
InterleaveSystem24.exe "[infile].img" "[outfile].s24" 2D00
```
IE, in my case, I used the following:
```shell
InterleaveSystem24.exe "ds3-5000-04d.img" "ds3-5000-04d.s24" 2D00
```
This is to work around a current issue in SAMdisk 3.8.10. SAMdisk uses the .s24 file extension to specifically detect Sega System 24 disk images, and understands what sector layout to use for these disks. Unfortunately, the way this is currently implemented assumes ".s24" images are laid out with all sectors from head 0 first, then all sectors from head 1, which doesn't match the expectations of MAME or the actual system. This simple program converts the image format for you. Note that the third parameter specifies the track size in bytes. This is 0x2D00 for all known games, except Hotrod and Scramble Spirits for Japan, which use 0x2F00. You need to set this number correctly for the game you're converting. Track sizes for each game can be found in the [MAME source](https://github.com/mamedev/mame/blob/master/src/mame/drivers/segas24.cpp).
8. Write the image to the disk using the following command (note the source file must have a .s24 extension):
```shell
samdisk.exe "[infile].s24" a:
```
IE, I used the following:
```shell
samdisk.exe "ds3-5000-04d.s24" a:
```

That's it! You now have a working floppy for the Sega System 24.


# Creating a MAME-compatible disk image using Anadisk
The System 24 disk images in MAME were originally created in a DOS program called Anadisk. Naturally this is where I went first to see if I could write the images back, but surprisingly, you can't write disk images using Anadisk. You can format a disk, and you can specify the unusual sector layout the Sega System 24 uses to store the game data, but there's no option to load data into the formatted sectors. You'd have to type it out byte by byte in the sector editor. Although Sydex, the creators of Anadisk, made a variety of other disk utilities, none of them appear to be able to work with the non-uniform sector sizes used in System 24 disks. The Windows-based utilities above offer a better alternative to working with System 24 disks in every way, however for completeness, here are the steps to create a raw sector image of a System 24 disk in Anadisk:

1. Download Anadisk 2.07 ([direct](Utilities/anadisk-207.zip)). Do NOT use 2.06. I discovered this version has a bug which resulted in sectors filled with 0xFF being read back incorrectly.
2. Load Anadisk onto a DOS-based PC with a 1.44MB FDD installed
3. Run anadisk.exe
4. Select "Dump" from the main menu
5. Configure to dump both sides, no sector ID information, enter a filename, and press enter.
6. Enter the start cylinder as "0", and press enter.
7. Enter the end cylinder as "79", and press enter. Alternatively, you can enter a larger number (IE, 255) to "overdump" the disk and extract all readable sectors.

That's it! The file will be written out to disk.
