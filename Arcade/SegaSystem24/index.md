---
layout: default
title: "Sega System 24"
pageGroup: "SegaSystem24"
---

# Sega System 24
![](ReferenceImage.png)

# Status
This system has been acquired in a working state for hardware testing and analysis, with both FDD and ROM board support. The 834-6510 I/O board has not currently been acquired.

# Overview
Refer to [SegaRetro](https://segaretro.org/Sega_System_24) and [System16](http://www.system16.com/hardware.php?id=708) for a general hardware overview and list of games.

# Documentation

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;Document&nbsp;&nbsp;&nbsp;&nbsp; | Author | Description | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Source&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
--- | --- | --- | --- | ---
| {% include download title="Official Schematics" target="Schematics/System24.pdf" %} | Sega | Complete hardware schematics for the System 24 main board, FDD controller board, and 834-6510 I/O board. These are the original schematics from Sega. The ROM board is not included in these schematics, however the pinout to the connector used by the ROM board is included, and the ROM board is relatively simple to trace. The schematics are clean and fairly easy to read. | Unknown
| {% include download title="834-6510 Schematic" target="Schematics/Capture3.JPG" %} | Apocalypse | Low-res schematic for a reproduction 834-6510 I/O board. As official schematics are available from Sega, look there first. | [arcadefixer.blogspot.com](http://arcadefixer.blogspot.com/2018/05/sega-834-6510-io-board-reproduction.html)<br>[www.arcade-projects.com](https://www.arcade-projects.com/forums/index.php?thread/5503-sega-834-6510-i-o-board-reproduction/)
4201-0004 | {% include download title="Scramble Spirits Owner's Manual" target="Manuals/4201-0004 - Scramble Spirits - Owner's Manual.pdf" %} | Sega | Owner's manual for Scramble Spirits, with wiring diagram. FDD version. | [www.gamesdatabase.org](http://www.gamesdatabase.org/all_manuals)
4201-0006 | {% include download title="CrackDown Owner's Manual" target="Manuals/4201-0006 - Crackdown - Owner's Manual.pdf" %} | Sega | Owner's manual for CrackDown, with wiring diagram. FDD version. | [www.gamesdatabase.org](http://www.gamesdatabase.org/all_manuals)
4201-5000-3D | {% include download title="Gain Ground Owner's Manual" target="Manuals/4201-5000-3D - Gain Ground - Owner's Manual.pdf" %} | Sega | Owner's manual for Gain Ground, with wiring diagram. FDD version. | [www.gamesdatabase.org](http://www.gamesdatabase.org/all_manuals)
420-0009U | {% include download title="Super Masters Owner's Manual" target="Manuals/420-0009U - Super Masters - Owner's Manual.pdf" %} | Sega | Owner's manual for Super Masters, with wiring diagram. FDD version. | [www.gamesdatabase.org](http://www.gamesdatabase.org/all_manuals)

# High Resolution Photos
##TODO##

# Miscellaneous Photos
Here's a collection of images of this system that have been posted online from various sources: {% include folder title="Hardware Photos" target="Pictures" %}
