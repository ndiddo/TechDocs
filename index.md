---
layout: default
title: "Exodus Technical Documentation"
---

**This site is still in early development, and many areas of the site are currently empty.**

The primary purpose of this site is to provide detailed technical information for a variety of legacy console and arcade hardware. This site currently has a specific focus around Sega platforms, however this may change in the future. The intended target audience are programmers who are interested in emulating these systems for the purpose of preservation, as well as developers who may be interested in modifying or developing new games or software which run on these platforms. This site aims to pull in every available piece of information from a wide variety of sources, appropriately grouped, categorized, and summarized, so that someone with enough technical background to understand the material can locate the best information as quickly and easily as possible. Some of the documents this site aims to provide are as follows:
- Software development kits
- Programmer manuals
- Service manuals
- System schematics
- Compilation tools
- Original source code
- High resolution board images
- Datasheets
- IC pinouts
- IC die scans

In addition to formal documentation from official sources, original research is also included from a variety of independent sources. In many cases official documentation is unavailable, limited, or contains errors, so pulling in original research to expand, correct, or clarify information from official sources is essential. As external sites often change or go offline, research and programs will usually be copied here rather than simply linked, to ensure that this can function as an independent, self-contained reference.

This site is not a wiki, however it is open for external contributions. The main site content is hosted in a public Git repo at <https://gitlab.com/ExodusWeb/TechDocs>, and is written in basic markdown. Large files are hosted externally, currently on [Google Drive](https://drive.google.com/drive/u/3/folders/1011Us5E-AvRVPPhTkg7gdDacz5CbqP-M). It is intended and encouraged for you to clone the entire site through these channels, so that you can use this site as an offline resource. If you want to contribute, fork the git repo and submit a merge request, with large files referenced externally.

Please note that some documents and tools hosted on this site may have originally been of a confidential nature, however in all such cases we believe the works have been legally obtained. Additionally, we assert that copyrighted works hosted here stand as abandoned by their original owners, and consider the sharing of this material here for research and archival purposes as exempt from copyright restrictions under the fair use category.