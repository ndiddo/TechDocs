module Jekyll
  module Tags
    class DetailsTagStart < Liquid::Tag

      def initialize(tag_name, markup, tokens)
        super
        @caption = markup
      end

      def render(context)
        site = context.registers[:site]
        converter = site.find_converter_instance(::Jekyll::Converters::Markdown)
        caption = converter.convert(@caption).gsub(/<\/?p[^>]*>/, '').chomp
        "{::nomarkdown}<details><summary>#{caption}</summary>{:/}"
      end

    end
  end
end

Liquid::Template.register_tag('detailsStart', Jekyll::Tags::DetailsTagStart)
