module Jekyll
  module Tags
    class DetailsTagEnd < Liquid::Tag

      def initialize(tag_name, markup, tokens)
        super
      end

      def render(context)
        "{::nomarkdown}</details>{:/}"
      end

    end
  end
end

Liquid::Template.register_tag('detailsEnd', Jekyll::Tags::DetailsTagEnd)
