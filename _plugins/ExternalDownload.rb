class ExternalDownloadTag < Liquid::Tag
  def initialize(tag_name, input, tokens)
    super
    @input = input
  end

  def render(context)
    escapedName = escape_name(@input)

    # Write the output HTML string
    output =  "#{escapedName}"

    # Render it on the page by returning it
    return output;
  end

  def escape_name(rawName)
    rawName
  end
end
Liquid::Template.register_tag('ed', ExternalDownloadTag)
