---
layout: default
title: "Sega Mega Drive (Genesis)"
pageGroup: "SegaMegaDrive"
---

# Sega Mega Drive (Genesis)
![](MegaDrive1.jpg)![](MegaDrive2.jpg)

# Overview
The Sega Mega Drive (known as Genesis in North America) is Sega's most successful video game console. For general information about this system and its games, please refer to other sources such as [segaretro.org](https://segaretro.org/Sega_Mega_Drive). This site focuses on the technical details of the system that are important for emulation and development, and assumes a familiarity with the general and historical facts of the console.

# Sections
{% include contents pageGroup=page.pageGroup %}
