---
layout: default
title: "Sega Saturn"
pageGroup: "SegaSaturn"
---

# Sega Saturn
![](SegaSaturn.jpg)

# Overview
The Sega Saturn is the follow-up to Sega's very successful Mega Drive (Genesis), however the Saturn saw mixed results, being popular in Japan but largely a commercial failure in other markets. For general information about this system and its games, please refer to other sources such as [segaretro.org](https://segaretro.org/Sega_Saturn). This site focuses on the technical details of the system that are important for emulation and development, and assumes a familiarity with the general and historical facts of the console.

# Sections
{% include contents pageGroup=page.pageGroup %}
